@extends('layouts.fixed-sidebar')

@section('content-title')
Daftar Berkas
@endsection

@section('content')
@include('flash::message')

<form action="{{route('set-tipe')}}" method="post">
  {!! csrf_field() !!}
  <div class="row">
    <div class="col-md-8">
      <div class="form-group">
        <label>Tipe Produk</label>
        <select class="form-control select2bs4 setTipe" name="tipe_produk_id" style="width: 80%;" onchange="this.form.submit()">
          <option value="0">Pilih Semua</option>
          @foreach($daftarTipe as $id=>$tipe)
            <option value="{{$id}}" {{$id == session('tipeId') ? 'selected' : ''}}>{{$tipe}}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
</form>


<div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                     <th>Nomor</th>
                            <th>Nama Berkas</th>
                            <th>Keterangan</th>
                            <th>Tipe Produk</th>
                            <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1; ?>
                        @foreach($daftarBerkas as $berkas)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $berkas->nama }}</td>
                                <td>{{ $berkas->keterangan }}</td>
                                <td>{{ $berkas->tipeProduk->nama }}</td>
                                <td>
                                    <a href="{{ url('lihat/' . $berkas->id) }}"><i class="fa fa-eye"></i> Lihat&nbsp;&nbsp;</a>
                                    <a href="{{ url($berkas->path) }}"><i class="fa fa-download"></i> Unduh</a>
                                </td>
                            </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
@stop
