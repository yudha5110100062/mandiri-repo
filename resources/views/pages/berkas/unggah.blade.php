@extends('layouts.fixed-sidebar')

@section('content-title')
Unggah Berkas
@endsection

@section('content')
 <div class="card card-primary">
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" method="post" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <div class="card-body">
      <div class="form-group">
        <label for="namaBerkas">Nama Berkas</label>
        <input type="text" class="form-control" id="namaBerkas" name="nama" placeholder="Masukkan nama berkas" required>
      </div>
      <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Masukkan keterangan berkas">
      </div>
      <div class="form-group">
        <label>Tipe Produk</label>
        <select class="form-control select2bs4" name="tipe_produk_id" style="width: 100%;">
          @foreach($daftarTipe as $id=>$tipe)
            <option value="{{$id}}">{{$tipe}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="exampleInputFile">Berkas</label>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas" required>
            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
          </div>
          <div class="input-group-append">
            <span class="input-group-text" id="">Upload</span>
          </div>
        </div>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>

@stop
