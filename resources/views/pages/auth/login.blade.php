<!DOCTYPE html>
<html>

@include('layouts.partials.html-head')

<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition login-page">
@yield('start-body')
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-logo">
    <a href="../../index2.html"><b>ROCKET</b>APPS</a>
    
  </div>
 
  <div class="login-box-body">
    <form method="post">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 col-xs-offset-8">
          {!! csrf_field() !!}
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <br>
  @include('flash::message')
  <!-- /.login-box-body -->
</div>

@include('layouts.partials.foot-script')

@yield('end-body')
</body>
</html>
