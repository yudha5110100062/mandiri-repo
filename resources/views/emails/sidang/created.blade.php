<p>Yang kami hormati, {{ $mahasiswa->nama }} (NRP {{ $mahasiswa->nrp }})</p>
<p>Jadwal ujian {{ $jenisSidang->nama }} Anda telah ditentukan. Berikut rincian jadwal pelaksanaan ujian.</p>
<ul>
	<li>Tanggal: {{ $sidang->tanggal->format('d-m-Y') }}</li>
	<li>Waktu: {{ $sidang->mulai }} - {{ $sidang->selesai }}</li>
	<li>Ruang Sidang: {{ $ruangSidang->nama }}</li>
	<li>Dosen Penguji:
		<ol>
		@foreach ($penguji as $dosen)
			<li>{{ $dosen->nama }}</li>
		@endforeach
		</ol>
	</li>
</ul>
<p>Demikian informasi ini kami sampaikan. Kami mengharapkan hasil yang terbaik dari Anda.</p>
<p>Terima kasih.</p>
<p>Ttd, Manajemen TERAS ITS</p>
