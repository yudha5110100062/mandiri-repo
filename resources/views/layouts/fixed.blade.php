<!DOCTYPE html>
<html>

@include('layouts.partials.html-head')

<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-teras fixed sidebar-mini">
@yield('start-body')
<!-- Site wrapper -->
<div class="wrapper">

  @include('layouts.partials.main-header')

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  @include('layouts.partials.sidebar')

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @include('layouts.partials.content-header')

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        @yield('content')
    </section><!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('layouts.partials.footer')

</div>
<!-- ./wrapper -->

@include('layouts.partials.foot-script')

@yield('end-body')
</body>
</html>
