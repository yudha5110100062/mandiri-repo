@section('end-head')
<link rel="stylesheet" href="{{ url() }}/plugins/select2/select2.min.css">
<link rel="stylesheet" href="{{ url() }}/plugins/select2/select2-bootstrap.min.css">
@append

@section('end-body')
<script type="text/javascript" src="{{ url() }}/plugins/select2/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	$('.select2').select2({
    		theme: 'bootstrap'
    	});
    });
</script>
@append