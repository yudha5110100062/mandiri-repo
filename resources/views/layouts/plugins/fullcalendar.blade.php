@section('end-head')
<link rel='stylesheet' href='{{ url('/plugins/fullcalendar/fullcalendar.css') }}' />
@append

@section('end-body')
<script src='{{ url('/plugins/daterangepicker/moment.min.js') }}'></script>
<script src='{{ url('/plugins/fullcalendar/fullcalendar.js') }}'></script>
@append