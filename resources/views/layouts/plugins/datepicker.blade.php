@section('end-head')
<link rel="stylesheet" href="{{ url() }}/plugins/datepicker/datepicker3.css">
@append

@section('end-body')
<script type="text/javascript" src="{{ url() }}/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	$('.datepicker').datepicker({
    		format: 'yyyy-mm-dd',
    		autoclose: true
    	});
    });
</script>
@append