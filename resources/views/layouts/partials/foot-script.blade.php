<!-- jQuery 2.2.0 -->
<!-- <script src="{{ url() }}/plugins/jQuery/jQuery-2.2.0.min.js"></script> -->
<script src="{{ url() }}/js/jquery-1.11.2.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ url() }}/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="{{ url() }}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{ url() }}/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{ url() }}/js/app.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
	    $(':required').parents('.form-group').addClass('required');
	});
</script>
