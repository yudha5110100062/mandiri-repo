<header class="main-header">
  <!-- Logo -->
  <a href="{{ url('/') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">{{ env('APP_NAME') }}</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">{{ env('APP_NAME') }}</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        @if (Auth::guest())
        <li>
          <a href="{{ url('auth/login') }}">Login</a>
        </li>
        @else        
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="hidden-xs">{{ Auth::user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ url('auth/profile') }}" class="btn btn-default btn-flat"><span class="fa fa-user"></span> Profil</a>
              </div>
              <div class="pull-right">
                <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat"><span class="fa fa-power-off"></span> Logout</a>
              </div>
            </li>
          </ul>
        </li>
        @endif
      </ul>
    </div>
  </nav>
</header>
