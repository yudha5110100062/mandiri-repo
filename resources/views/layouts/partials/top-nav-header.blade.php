<header class="main-header">
  <nav class="navbar navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <a href="{{ url('/') }}" class="navbar-brand">{{ env('APP_NAME') }}</a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          @if (Auth::guest())
          @else
          @foreach (Auth::user()->sidebarMenu() as $menu)
              @if ($menu->children->count())
                  <li class="dropdown">
                      <a href="{{ url($menu->url) }}" class="dropdown-toggle" data-toggle="dropdown"><span>{{ $menu->name }}</span> <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                      @foreach ($menu->children as $child)                          
                          <li><a href="{{ url($child->url) }}"><i class="{{ $child->icon }}"></i> {{ $child->name }}</a></li>
                      @endforeach
                      </ul>
                  </li>
              @else
                  <li><a href="{{ url($menu->url) }}"><span>{{ $menu->name }}</span></a></li>
              @endif
          @endforeach
          @endif  
        </ul>
      </div>
      <!-- /.navbar-collapse -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          @if (Auth::guest())
          <li>
            <a href="{{ url('auth/login') }}">Login</a>
          </li>
          @else        
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('auth/profile') }}" class="btn btn-default btn-flat"><span class="fa fa-user"></span> Profil</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat"><span class="fa fa-power-off"></span> Logout</a>
                </div>
              </li>
            </ul>
          </li>
          @endif
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
      <!-- /.navbar-custom-menu -->
    </div>
    <!-- /.container-fluid -->
  </nav>
</header>