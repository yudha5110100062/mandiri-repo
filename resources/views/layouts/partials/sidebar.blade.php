<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li>
                <a href="#">
                    <i class="fa fa-calendar"></i> <span>{{ strftime("%A, %d %B %Y") }}</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-clock-o"></i> <span>{{ date("H:i") }}</span>
                </a>
            </li> 
            <li class="header">
                <a href="{{ url('index') }}">Daftar Berkas</a>
                @if(session('user')->is_admin)
                <a href="{{ url('unggah') }}">Unggah Berkas</a>
                @endif
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
