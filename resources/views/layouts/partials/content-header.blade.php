<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('content-title', 'Page Header here')
        <small>@yield('content-description')</small>
    </h1>
    <ol class="breadcrumb">
        @yield('breadcrumb')
    </ol>
</section>
