<!DOCTYPE html>
<html>

@include('layouts.partials.html-head')

<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-teras layout-top-nav">
@yield('start-body')
<div class="wrapper">

  @include('layouts.partials.top-nav-header')

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      @include('layouts.partials.content-header')

      <!-- Main content -->
      <section class="content">
          <!-- Your Page Content Here -->
          @yield('content')
      </section><!-- /.content -->
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  @include('layouts.partials.footer')
</div>
<!-- ./wrapper -->

@include('layouts.partials.foot-script')

@yield('end-body')
</body>
</html>
