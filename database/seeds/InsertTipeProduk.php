<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InsertTipeProduk extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $daftarTipe = [
        	'Tabungan Bisnis','Tabungan Reguler','Tabungan Mikro','Tabungan Flexi Payroll','Tabungan Now','Tabungan Simpel','Tabungan SiMakmur','Tabungan Premium','Tabungan Rencana Mandiri','Giro Perorangan','Giro  Perusahaan','Giro Pemerintah','Giro Premium','Deposito On Call (DOC)','Deposito Reguler','Mandiri Deposito Swap','Mandiri Dual Currency Investment','INVESTASI - Reksadana (Pasar Uang, Pendapatan Tetap Campuran, Terproteksi, Saham)','INVESTASI - Surat Berharga (Obligasi)','INVESTASI - Bancassurance (Unit Link)','INVESTASI - Saham','INVESTASI - Derivatif','TREASURY - FX Forward','TREASURY - FX Swap','TREASURY - FX Option','TREASURY - Call Spread','TREASURY - Interest Rate Swap','TREASURY - Cross Currency Swap','TREASURY - FX DNDF','ECHANNEL - Mandiri Online','ECHANNEL - Mandiri Internet Bisnis','ECHANNEL - Mandiri Cash Management','ECHANNEL - EDC','ECHANNEL - Branchless Banking','ECHANNEL - E-COMMERCE - Digi Retail','ECHANNEL - E-COMMERCE - Digiresto','ECHANNEL - E-COMMERCE - QRIS','ECHANNEL - Mandiri Virtual Account','ECHANNEL - Supply Chain Management','ECHANNEL - Linkaja','ECHANNEL - SMS Banking','KREDIT - MIKRO - KSM','KREDIT - MIKRO - KUM','KREDIT - MIKRO - KUR','KREDIT - SME - KMK','KREDIT - SME - KI','KREDIT - SME - KASB','KREDIT - SME - Kredit Talangan','KREDIT - SME - Supplier Financing','KREDIT - SME - Bank Garansi/LC/SKBDN','KREDIT - CONSUMER - KPR','KREDIT - CONSUMER - KKB','KREDIT - CONSUMER - Kartu Kredit','KREDIT - COMMERCIAL - KMK','KREDIT - COMMERCIAL - KI','KREDIT - COMMERCIAL - Invoice Financing','KREDIT - COMMERCIAL - LC/SKBDN','KREDIT - COMMERCIAL - Bill Processing Line','SOLUSI - SECTOR SOLUTION - Healthcare','SOLUSI - SECTOR SOLUTION - Education ','SOLUSI - SECTOR SOLUTION - Port','SOLUSI - SECTOR SOLUTION - Mining','SOLUSI - BANK GARANSI - Bank Garansi Jaminan Tender/Penawaran','SOLUSI - BANK GARANSI - Bank Garansi Jaminan Uang Muka','SOLUSI - BANK GARANSI - Bank Garansi Jaminan Pelaksanaan','SOLUSI - BANK GARANSI - Bank Garansi Jaminan Pemeliharaan','SOLUSI - BANK GARANSI - Counter Bank Guarantee','SOLUSI - Trade Service','SOLUSI - Trade Finance','SOLUSI - Treasury','SOLUSI - LIQUIDITY MANAGEMENT - National Pooling ','SOLUSI - RECEIVABLE MANAGEMENT - Mandiri Bill Collection','SOLUSI - RECEIVABLE MANAGEMENT - Mandiri Auto Debit','SOLUSI - RECEIVABLE MANAGEMENT - SOPP Pertamina','SOLUSI - RECEIVABLE MANAGEMENT - Modern Channel','SOLUSI - RECEIVABLE MANAGEMENT - Cash & Pick Up Delivery','SOLUSI - PAYMENT MANAGEMENT - Host to Host Payment','SOLUSI - PAYMENT MANAGEMENT - MCM','SOLUSI - PAYMENT MANAGEMENT - e-Tax','SOLUSI - PAYMENT MANAGEMENT - Smart Account','SOLUSI - PAYMENT MANAGEMENT - Corporate SWIFT','SOLUSI - PAYMENT MANAGEMENT - Coroporate Card','SOLUSI - PAYMENT MANAGEMENT - MMTS','SOLUSI - PAYMENT MANAGEMENT - MFT','LAYANAN PRIORITAS - Benefit','LAYANAN PRIORITAS - Fasilitas','PRICING - Biaya Produk','PRICING - Biaya Layanan','PRICING - Suku Bunga','PRICING - Special Rate','PROGRAM - MIKRO - Semarak Rakyat','PROGRAM - MIKRO - Agen Jempolan','PROGRAM - PEBISNIS - Nabung Cerdas','PROGRAM - PEBISNIS - MTB Gratis RTGS/SKN','PROGRAM - PRIORITAS - Tabungan Premium','PROGRAM - PERUSAHAAN - Giro SME','PROGRAM - PERUSAHAAN - Giro Commercial','PROGRAM - PERUSAHAAN - Giro MCM MAXI','PROGRAM - PERUSAHAAN - INTAX (Incentives for Tax Payer)','PROGRAM - PERUSAHAAN - Rekening Khusus Devisa Hasil Ekspor Sumber Daya Alam','PROGRAM - INSTANSI - Rumah Sakit','PROGRAM - INSTANSI - Perguruan Tinggi Swasta','PROGRAM - INSTANSI - Port Sector'
        ];

        foreach ($daftarTipe as $item) {
        	DB::table('tipe_produk')->insert([
        		'nama' => $item
        	]);
        }
    }
}
