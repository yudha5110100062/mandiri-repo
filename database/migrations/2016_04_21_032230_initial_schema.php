<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class InitialSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',100)->unique();
            $table->string('password');
            $table->string('name');
            $table->boolean('is_admin');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('berkas', function (Blueprint $table){
            $table->increments('id');
            $table->string('path');
            $table->string('nama')->nullable();
            $table->string('ext')->nullable()->comment('ekstensi file');
            $table->string('keterangan')->nullable();
            $table->unsignedInteger('diunggah_oleh');
            $table->timestamps();
        });

        Schema::table('berkas', function (Blueprint $table){
            $table->foreign('diunggah_oleh')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('berkas', function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });

        Schema::drop('users');
        Schema::drop('berkas');
    }
}
