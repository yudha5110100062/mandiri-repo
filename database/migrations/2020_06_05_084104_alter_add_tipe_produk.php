<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddTipeProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipe_produk', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama');
        });

        Schema::table('berkas', function(Blueprint $table){
            $table->unsignedInteger('tipe_produk_id');
            $table->foreign('tipe_produk_id')->references('id')->on('tipe_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('berkas', function(Blueprint $table){
            $table->dropForeign(['tipe_produk_id']);
            $table->dropColumn('votes');
        });

        Schema::drop('tipe_produk');
    }
}
