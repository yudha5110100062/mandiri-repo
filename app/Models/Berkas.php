<?php

namespace App\Models;

use App\Models\TipeProduk;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Berkas extends Model
{
    protected $table = 'berkas';

    public function pengunggah()
    {
    	return $this->belongsTo(User::class, 'diunggah_oleh');
    }

    public function tipeProduk()
    {
    	return $this->belongsTo(TipeProduk::class, 'tipe_produk_id', 'id');
    }
}
