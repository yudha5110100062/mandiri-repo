<?php

namespace App\Models;

use Profio\Auth\Role as BaseRole;

class Role extends BaseRole
{
    public static function getByName($name)
    {
        return self::where('name', $name)->first();
    }
}
