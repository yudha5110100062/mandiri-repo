<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    public function upload(UploadedFile $file, $destinationPath)
    {
        $fileName = $file->getClientOriginalName();

        $file->move($destinationPath, $fileName);

        $savedPath = $destinationPath . $fileName;

        return $savedPath;
    }
}
