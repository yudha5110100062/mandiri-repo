<?php
namespace App\Extensions\AdamWathan\Form;

use App\Extensions\AdamWathan\Form\Elements\Select;
use AdamWathan\Form\FormBuilder as BaseFormBuilder;

class FormBuilder extends BaseFormBuilder
{
	public function select($name, $options = [])
	{
	    $select = new Select($name, $options);

	    $selected = $this->getValueFor($name);
	    $select->select($selected);

	    return $select;
	}
}