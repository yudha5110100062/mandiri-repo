<?php
namespace App\Extensions\AdamWathan\Form\Elements;

use AdamWathan\Form\Elements\Select as BaseSelect;

class Select extends BaseSelect
{
	protected $placeholderLabel = null;

	protected $placeholderValue = null;

	public function placeholder($label, $value = null)
	{
		$this->placeholderLabel = $label;
		$this->placeholderValue = $value;
	}

	protected function renderOptions()
	{
	    list($values, $labels) = $this->splitKeysAndValues($this->options);

	    $tags = array_map(function ($value, $label) {
	        if (is_array($label)) {
	            return $this->renderOptGroup($value, $label);
	        }
	        return $this->renderOption($value, $label);
	    }, $values, $labels);
	    
	    $options = implode($tags);

	    if (!is_null($this->placeholderLabel)) {
	    	$options = '<option value="" disabled="disabled" selected="selected">- ' . $this->placeholderLabel . ' -</option>' . $options;
	    }

	    return $options;
	}
}