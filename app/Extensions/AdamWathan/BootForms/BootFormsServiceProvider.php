<?php

namespace App\Extensions\AdamWathan\BootForms;

use AdamWathan\BootForms\BootFormsServiceProvider as BaseServiceProvider;
use App\Extensions\AdamWathan\Form\FormBuilder;

class BootFormsServiceProvider extends BaseServiceProvider
{
	protected function registerFormBuilder()
	{
	    $this->app['adamwathan.form'] = $this->app->share(function ($app) {
	        $formBuilder = new FormBuilder;
	        $formBuilder->setErrorStore($app['adamwathan.form.errorstore']);
	        $formBuilder->setOldInputProvider($app['adamwathan.form.oldinput']);
	        $formBuilder->setToken($app['session.store']->getToken());

	        return $formBuilder;
	    });
	}
}