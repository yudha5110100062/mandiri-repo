<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Berkas;
use App\Models\TipeProduk;
use Illuminate\Http\Request;

class BerkasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        // dd(session('tipeId'));
        if (session('tipeId') === null || session('tipeId') == 0) {
            $daftarBerkas = Berkas::with('tipeProduk')->get();
        } else {
            $daftarBerkas = Berkas::with('tipeProduk')->where('tipe_produk_id', session('tipeId'))->get();
        }
        $daftarTipe = TipeProduk::get()->pluck('nama', 'id');
        return view('pages.berkas.index', compact('daftarBerkas', 'daftarTipe'));
    }

    public function getUnggah()
    {
        if (!session('user')->is_admin) {
            flash()->warning('Anda tidak memiliki akses ke halaman tersebut.');
            return redirect('index');
        }

        $daftarTipe = TipeProduk::get()->pluck('nama', 'id');

        return view('pages.berkas.unggah', compact('daftarTipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postUnggah(Request $request)
    {
        $file = $request->file('berkas');
        $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $fileExt = $file->getClientOriginalExtension();
        $fileOri = $file->getClientOriginalName();
        $filePath = 'upload/';

        $file->move($filePath, $fileOri);
        $berkas = new Berkas;
        $berkas->path = $filePath . $fileOri;
        $berkas->nama = $request->nama;
        $berkas->ext = $fileExt;
        $berkas->keterangan = $request->keterangan;
        $berkas->pengunggah()->associate(session('user'));
        $berkas->TipeProduk()->associate(request('tipe_produk_id'));

        $berkas->save();

        flash()->success('Berkas berhasil diunggah.');

        return redirect('index');
    }

    public function lihat($id)
    {
        $file = Berkas::find($id);
        if ($file->ext == 'pdf') {
            header("Content-type: application/pdf");
            header("Content-Disposition: inline; filename=filename.pdf");
            @readfile($file->path);
        } elseif($file->ext == 'jpg')
            header("Content-type: image/jpg");
            header("Content-Disposition: inline; filename=filename.jpg");
            @readfile($file->path);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
