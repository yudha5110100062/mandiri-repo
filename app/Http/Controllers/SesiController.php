<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SesiController extends Controller
{
    public function setTipe()
    {
        session(['tipeId' => request('tipe_produk_id')]);
        return redirect('index');
    }
}
