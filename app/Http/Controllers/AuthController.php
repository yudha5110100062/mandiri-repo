<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Profio\Auth\User;

class AuthController extends Controller
{
    function getLogin()
    {
        return view('pages.auth.login');
    }

    function postLogin()
    {
        return $this->login(request()->get('username'));
    }

    function getLogout()
    {
        Session::flush();
        Auth::logout();
        return redirect('/auth/login');
    }

    private function login($username)
    {
        $user = User::where('username', $username)->first();
        if (!is_null($user)) {
            Auth::loginUsingId($user->id);
            session(['user' => $user]);
            
            return redirect()->intended('index');
        } else {
            flash()->warning('User tidak ditemukan!');
            return redirect()->back();
        }
    }
}
