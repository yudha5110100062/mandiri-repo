<?php

Route::get('/', function () {
    return redirect('/auth/login');
});
Route::get('/auth/login', [
    'as'   => 'auth.login',
    'uses' => 'AuthController@getLogin',
]);
Route::post('/auth/login', [
    'as'   => 'auth.login',
    'uses' => 'AuthController@postLogin',
]);
Route::get('/auth/logout', [
    'as'   => 'auth.logout',
    'uses' => 'AuthController@getLogout',
]);
Route::get('index', [
    'as'   => 'index',
    'uses' => 'BerkasController@getIndex',
]);
Route::get('unggah', [
    'as'   => 'unggah',
    'uses' => 'BerkasController@getUnggah',
]);
Route::post('unggah', [
    'as'   => 'unggah',
    'uses' => 'BerkasController@postUnggah',
]);
Route::get('lihat/{id}', [
    'as'   => 'lihat',
    'uses' => 'BerkasController@lihat',
]);
Route::post('set-tipe', [
    'as' => 'set-tipe',
    'uses' => 'SesiController@setTipe'
]);