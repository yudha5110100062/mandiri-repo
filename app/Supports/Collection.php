<?php
namespace App\Supports;

use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{
    public function combine()
    {
        $combined = new BaseCollection;

        $itemCount = count((new static($this->items))->first());
        for ($i = 0; $i < $itemCount; $i++) {
        	$item = new BaseCollection;
            foreach ($this->items as $key => $value) {
            	$item[$key] = $value[$i];
            }
        	$combined->push($item);
        }

        return $combined;
    }
}
